# `labotekh.labotekh.reverse_proxy`
The `labotekh.labotekh.reverse_proxy` role deploy and a Nginx and a Certbot container. The first one will act as a reverse proxy for the web services deployed on the host and the second one will handle the TLS/SSL certificate generation for root domain and web service specific domains.

The Nginx container will send the web requests to the web service depending on the FQDN. These request will be forwarded to the web service's container on the port 8000 through a dedicated virtual Docker network. The Nginx container will also have access to the `<root_directory>/<service_name>/files` in order to serve static and media files.

A wildcard TLS/SSL wildcard certificate is generated only for the root domain of the host. All root domains must be owned by the same OVH account and the API credentials of this account must be provided on the variables `ovh.dns_ovh_endpoint`, `ovh.dns_ovh_application_key`, `ovh.dns_ovh_application_secret` and `ovh.dns_ovh_consumer_key`. Otherwise the HTTPS won't be enabled for the services. Root domain certificates are renewed by a weekly cronjob.
